//
//  AppDelegate.h
//  test123
//
//  Created by David Chiu on 20/11/2018.
//  Copyright © 2018 David Chiu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

